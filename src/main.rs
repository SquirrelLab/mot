extern crate chrono;
extern crate chrono_tz;

use std::{io};
use std::time::{Instant, Duration};
use chrono::{DateTime, Utc, TimeZone};
use crossterm::{event, execute};
use crossterm::event::{Event, KeyCode, EnableMouseCapture, DisableMouseCapture};
use crossterm::terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen};
use tui::backend::{Backend, CrosstermBackend};
use tui::{Frame, Terminal};
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::widgets::{Block, Borders, BorderType, List, ListItem, ListState};

use chrono_tz::US::Samoa; // UTC-11
use chrono_tz::US::Hawaii; // UTC-10
use chrono_tz::Pacific::Marquesas; // UTC-9:30
use chrono_tz::US::Alaska; // UTC-9
use chrono_tz::US::Pacific; // UTC-8
use chrono_tz::US::Mountain; //UTC-7
use chrono_tz::US::Central; //UTC-6
use chrono_tz::US::Eastern; //UTC-5
use chrono_tz::America::Barbados; //UTC-4
use chrono_tz::Canada::Newfoundland; //UTC-3:30
use chrono_tz::Brazil::East; //UTC-3
use chrono_tz::Brazil::DeNoronha; //UTC-2
use chrono_tz::Atlantic::Cape_Verde; //UTC-1
use chrono_tz::UTC;
use chrono_tz::Europe::Monaco; //UTC+1
use chrono_tz::Egypt; //UTC+2
use chrono_tz::Turkey; //UTC+3
use chrono_tz::Iran; //UTC+3:30
use chrono_tz::Europe::Samara; //UTC+4
use chrono_tz::Asia::Kabul; //UTC+4:30
use chrono_tz::Indian::Maldives; //UTC+5
use chrono_tz::Asia::Colombo; //UTC+5:30
use chrono_tz::Asia::Kathmandu; //UTC+5:45
use chrono_tz::Asia::Omsk; //UTC+6
use chrono_tz::Indian::Cocos; //UTC+6:30
use chrono_tz::Asia::Ho_Chi_Minh; //UTC+7
use chrono_tz::Hongkong; //UTC+8
use chrono_tz::Australia::Eucla; //UTC+8:45
use chrono_tz::Asia::Tokyo; //UTC+9
use chrono_tz::Australia::Broken_Hill; //UTC+9:30
use chrono_tz::Pacific::Guam; //UTC+10
use chrono_tz::Australia::Lord_Howe; //UTC+10:30
use chrono_tz::Asia::Magadan; //UTC+11
use chrono_tz::Pacific::Fiji; //UTC+12
use chrono_tz::Pacific::Chatham; //UTC+12:45
use chrono_tz::Tz;

struct TimeWatcher {
    timezone: String,
}

struct App {
    pub timezone_list_state: ListState,
    timezone_list: Vec<String>,
    pub modify_list_state: ListState,
    pub modify_list: Vec<String>,
    pub time_boxes: Vec<TimeWatcher>
}

impl App {
    pub fn new() -> Self {

        let utc = Utc::now();
        let mut timezone_name_list: Vec<String> = vec![];
        timezone_name_list.push(Samoa.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Hawaii.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Marquesas.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Alaska.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Pacific.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Mountain.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Central.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Eastern.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Barbados.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Newfoundland.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(East.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(DeNoronha.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Cape_Verde.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(UTC.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Monaco.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Egypt.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Turkey.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Iran.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Samara.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Kabul.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Maldives.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Colombo.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Kathmandu.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Omsk.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Cocos.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Ho_Chi_Minh.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Hongkong.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Eucla.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Tokyo.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Broken_Hill.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Guam.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Lord_Howe.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Magadan.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Fiji.timestamp(utc.timestamp(), 0).timezone().to_string());
        timezone_name_list.push(Chatham.timestamp(utc.timestamp(), 0).timezone().to_string());

        App {
            timezone_list_state: ListState::default(),
            timezone_list: timezone_name_list,
            modify_list_state: ListState::default(),
            modify_list: vec!["New".to_string(), "Delete".to_string()],
            time_boxes: vec![]
        }
    }
}

fn main() -> Result<(), io::Error> {

    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let tick_rate = Duration::from_millis(250);
    let mut app: App = App::new();
    let res = run_app(&mut terminal, tick_rate, &mut app);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    tick_rate: Duration,
    app: &mut App,
) -> io::Result<()> {
    let mut last_tick = Instant::now();
    loop {
        terminal.draw(|f| ui(f, app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                if let KeyCode::Char('q') = key.code {
                    return Ok(());
                }
                else if let KeyCode::Up = key.code {
                    if app.timezone_list_state.selected() == None {
                        app.timezone_list_state.select(Some(0))
                    }
                    else if app.timezone_list_state.selected().unwrap() == 0 {
                        app.timezone_list_state.select(Some(app.timezone_list.len() - 1))
                    }
                    else {
                        app.timezone_list_state.select(Some(app.timezone_list_state.selected().unwrap() - 1))
                    }
                }
                else if let KeyCode::Down = key.code {
                    if app.timezone_list_state.selected() == None {
                        app.timezone_list_state.select(Some(0))
                    }
                    else if app.timezone_list_state.selected().unwrap() == app.timezone_list.len() - 1 {
                        app.timezone_list_state.select(Some(0))
                    }
                    else {
                        app.timezone_list_state.select(Some(app.timezone_list_state.selected().unwrap() + 1))
                    }
                }

                else if let KeyCode::Left = key.code {
                    if app.modify_list_state.selected() == None {
                        app.modify_list_state.select(Some(0))
                    }
                    else if app.modify_list_state.selected().unwrap() == 0 {
                        app.modify_list_state.select(Some(app.modify_list.len() - 1))
                    }
                    else {
                        app.modify_list_state.select(Some(app.modify_list_state.selected().unwrap() - 1))
                    }
                }
                else if let KeyCode::Right = key.code {
                    if app.modify_list_state.selected() == None {
                        app.modify_list_state.select(Some(0))
                    }
                    else if app.modify_list_state.selected().unwrap() == app.modify_list.len() - 1 {
                        app.modify_list_state.select(Some(0))
                    }
                    else {
                        app.modify_list_state.select(Some(app.modify_list_state.selected().unwrap() + 1))
                    }
                }
                else if let KeyCode::Enter = key.code {
                    if app.modify_list_state.selected() != None && app.timezone_list_state.selected() != None {
                        let selected_timezone : usize = app.timezone_list_state.selected().unwrap();
                        if app.modify_list_state.selected().unwrap() == 0 {
                            let the_time_string: &String = app.timezone_list.iter().nth(selected_timezone).unwrap();
                            let the_time : Tz = the_time_string.parse().unwrap();

                            let timewatcher_test = TimeWatcher {
                                timezone: the_time.to_string(),
                            };

                            app.time_boxes.push(timewatcher_test);
                        }
                        else {
                            let timezone : Option<usize> = app.time_boxes.iter().position(|watcher| {
                                &watcher.timezone == app.timezone_list.iter().nth(selected_timezone).unwrap()
                            });
                            if timezone.is_some() {
                                app.time_boxes.remove(timezone.unwrap());
                            }
                        }
                    }
                }
            }
        }
        if last_tick.elapsed() >= tick_rate {
            last_tick = Instant::now();
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    let chunks_vertical = Layout::default()
        .direction(Direction::Vertical)
        .margin(2)
        .constraints([Constraint::Percentage(80), Constraint::Percentage(20)].as_ref())
        .split(f.size());

    let chunks_horizontal = Layout::default()
        .direction(Direction::Horizontal)
        .margin(2)
        .constraints([Constraint::Percentage(20) ,Constraint::Percentage(80)].as_ref())
        .split(chunks_vertical[0]);

    let panel_main = Block::default().borders(Borders::NONE);
    let panel_bottom = Block::default()
        .borders(Borders::ALL)
        .border_style(Style::default().fg(Color::White))
        .border_type(BorderType::Plain)
        .style(Style::default().bg(Color::Black));
    let panel_left = Block::default()
        .borders(Borders::ALL)
        .border_style(Style::default().fg(Color::White))
        .border_type(BorderType::Plain)
        .style(Style::default().bg(Color::Black));

    let timezone_items: Vec<ListItem> = app.timezone_list.iter().map(|name| ListItem::new(name.as_str())).collect();
    let timezone_ui_list = List::new(timezone_items).block(panel_left).highlight_symbol(">>").highlight_style(Style::default().add_modifier(Modifier::BOLD));

    let modify_items : Vec<ListItem> = app.modify_list.iter().map(|name| ListItem::new(name.as_str())).collect();
    let modify_ui_list = List::new(modify_items).block(panel_bottom).highlight_symbol(">>").highlight_style(Style::default().add_modifier(Modifier::BOLD));

    let constrains : Vec<Constraint> = app.time_boxes.iter().map( |_a_watcher|{Constraint::Percentage((1.0 / app.time_boxes.len() as f32 * 100 as f32) as u16)}).collect();
    let chunk_boxes = Layout::default()
        .direction(Direction::Horizontal)
        .margin(2)
        .constraints(constrains)
        .split(chunks_horizontal[1]);


    for (i, watcher) in app.time_boxes.iter().enumerate() {
        let block = Block::default()
            .borders(Borders::ALL)
            .border_style(Style::default().fg(Color::White))
            .border_type(BorderType::Plain)
            .style(Style::default().bg(Color::Black));

        let the_time : Tz = watcher.timezone.parse().unwrap();
        let a_time: DateTime<Tz> = the_time.timestamp(Utc::now().timestamp(), 0);
        let current_time = a_time.time().to_string();
        let time_info = List::new([ListItem::new(watcher.timezone.clone()), ListItem::new(current_time)]).block(block);
        f.render_widget(time_info, chunk_boxes[i]);
    };

    f.render_stateful_widget(timezone_ui_list, chunks_horizontal[0], &mut app.timezone_list_state);
    f.render_stateful_widget(modify_ui_list, chunks_vertical[1], &mut app.modify_list_state);

    f.render_widget(panel_main, chunks_vertical[0]);

}